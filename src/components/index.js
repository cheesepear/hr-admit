// 创建一个可以被 Vue.use 注册的组件
import PageTools from '@/components/PageTools'
import ImageUpload from '@/components/ImageUpload'
export default {
  install(Vue) {
    Vue.component('PageTools', PageTools)
    Vue.component('ImageUpload', ImageUpload)
  }
}

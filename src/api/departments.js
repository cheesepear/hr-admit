import request from '@/utils/request'

export function getDepartments() {
  return request({
    url: '/company/department'
  })
}

export function addDepartments(data) {
  return request({
    url: '/company/department',
    method: 'post',
    data
  })
}

export function delDepartments(id) {
  return request({
    url: '/company/department/' + id,
    method: 'delete'
  })
}

export function editDepartments(data) {
  return request({
    url: '/company/department/' + data.id,
    method: 'put',
    data
  })
}

export function getDepartmentDetails(id) {
  return request({
    url: '/company/department/' + id
  })
}

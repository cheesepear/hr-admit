import request from '@/utils/request'
// params是查询参数，里面需要携带分页信息
export function getRoleList(params) {
  return request({
    url: '/sys/role',
    params
  })
}

export function getCompanyDetail(companyId) {
  return request({
    url: '/company/' + companyId
  })
}

export function delRole(roleId) {
  return request({
    url: '/sys/role/' + roleId,
    method: 'delete'
  })
}

export function getRoleDetail(id) {
  return request({
    url: '/sys/role/' + id
  })
}

export function updateRole(data) {
  return request({
    method: 'put',
    url: '/sys/role/' + data.id,
    data
  })
}
export function addRole(data) {
  return request({
    url: '/sys/role',
    method: 'post',
    data
  })
}

import axios from 'axios'
import store from '@/store'
import { Message } from 'element-ui'
import { getTimeStamp } from './auth'
import router from '@/router'

// 设定失效时间
const timeout = 7200

const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // 设置axios请求的基础的基础地址
  timeout: 5000 // 定义5秒超时
})
service.interceptors.request.use(config => {
  // 在这个位置需要统一的去注入token
  if (store.getters.token) {
    // 如果token存在 注入token
    if (isCheckTimeout()) {
      // 如果超时了做三件事情
      // 1. 退出删除数据
      store.dispatch('user/logout')
      // 2. 跳到登录页
      router.push('/login')
      // 3. 返回一个错误
      return Promise.reject(new Error('token 超时'))
    } else {
      config.headers.Authorization = `Bearer ${store.getters.token}`
      // config.headers['Authorization'] = `Bearer ${store.getters.token}`
    }
  }
  return config // 必须返回配置
}, error => {
  return Promise.reject(error)
})
service.interceptors.response.use(response => {
  const { success, message, data } = response.data
  if (success) {
    return data
  } else {
    Message.error(message)
    return Promise.reject(new Error(message))
  }
}, error => {
  // 解开错误
  console.dir(error)
  // 处理token权限问题401的报错
  if (error.response && error.response.data && error.response.data.code === 10002) {
    store.dispatch('user/logout')
    router.push('/login')
  }
  // 处理状态码直接报错
  Message.error(error.message)
  return Promise.reject(error)
})

export default service

function isCheckTimeout(params) {
  // 这里封装判断是否过期的函数, 只会返回true/false
  // 当前时间戳
  const now = Date.now()
  const savedTime = getTimeStamp()
  // console.log(now)
  // console.log(savedTime)
  return (now - savedTime) / 1000 >= timeout
}

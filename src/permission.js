
import router from '@/router'
import store from '@/store'
import NProgress from 'nprogress' // 引入一份进度条插件
import 'nprogress/nprogress.css' // 引入进度条样式
const whiteList = ['/login', '/404']
router.beforeEach((to, from, next) => {
  NProgress.start()// 开启进度条
  if (store.getters.token) {
    if (to.path === '/login') {
      return next('/')
    } else {
      if (!store.getters.userId) {
        // 有数据就不管, 如果没数据就调用一下 action 获取数据
        store.dispatch('user/getUserInfo')
      }
      return next()
    }
  } else {
    if (whiteList.indexOf(to.path) > -1) {
      return next()
    } else {
      return next('/login')
    }
  }
  // NProgress.done() // 关闭进度条
})
router.afterEach(function() {
  NProgress.done() // 关闭进度条
})

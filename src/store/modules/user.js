import { login, getUserInfo, getUserDetailById } from '@/api/user'
import { getToken, setToken, removeToken, setTimeStamp } from '@/utils/auth'
// import { resetRouter } from '@/router'
import { Message } from 'element-ui'

export default {
  namespaced: true,
  state: {
    token: getToken(),
    userInfo: {}
  },
  mutations: {
    // 修改 token 的 mutations
    setToken(state, data) {
      // 1. 将传入的 token 放到 vuex state(方便使用, 但是不能持久化)
      state.token = data
      // 2. 引入了 utils 里面的小工具, 用来将token同时存放到 cookie (不方便使用, 但是可以持久化)
      setToken(data)
    },
    removeToken(state) {
      state.token = ''
      removeToken()
    },
    setUserInfo(state, data) {
      state.userInfo = data
    },
    removeUserInfo(state) {
      state.userInfo = {}
    }

  },
  actions: {
    // 'login': function() {
    login(context, data) {
      // 这里是一个异步请求，如果不return，就先执行handleLogin()
      // 正常流程是先设置token,有了token之后跳转到首页，如果先跳到首页再设置token，会出现报错
      return login(data).then(data => {
        console.log('action调用之后')
        // 1. 弹窗
        Message.success('登录成功')
        // 2. 调用 mutations
        context.commit('setToken', data)
        setTimeStamp()
      })
    },
    'getUserInfo': async(context) => {
      // 先发送用户基本信息请求
      const dataUserInfo = await getUserInfo()
      // 再利用 id 发送详情请求
      const dataUserDetail = await getUserDetailById(dataUserInfo.userId)
      // 是将两个请求的数据结合起来,一起放入 userInfo 里面去
      const userInfo = {
        ...dataUserInfo,
        ...dataUserDetail
      }
      context.commit('setUserInfo', userInfo)
    },
    logout(context) {
      // 1. 清理token
      const { commit } = context
      commit('removeToken')
      // 2. 清理userInfo
      commit('removeUserInfo')
    }
  }
}

import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import Component from '@/components'
import 'element-ui/lib/theme-chalk/index.css'
// import locale from 'element-ui/lib/locale/lang/en' // lang i18n

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'

import '@/icons' // icon
import '@/permission' // permission control

/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online ! ! !
 */
// if (process.env.NODE_ENV === 'production') {
//   const { mockXHR } = require('../mock')
//   mockXHR()
// }

// set ElementUI lang to EN
// Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
Vue.use(ElementUI)
Vue.use(Component)
import { imageerror } from '@/directives'

Vue.directive('imageerror', imageerror)
Vue.config.productionTip = false

// 引入所有过滤器
import * as filters from '@/filters'
// 全局过滤器的创建方法是 Vue.filter(名称, 函数体)
// console.log(filters)
// 拿到的是一个对象, 里面以 key: value 的形式储存了 过滤器名:过滤器函数
// 遍历这个对象
for (var key in filters) {
  const filterName = key
  const filterFunction = filters[key]
  Vue.filter(filterName, filterFunction)
}
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})

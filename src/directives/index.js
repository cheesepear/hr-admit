export const imageerror = {
  // 这个钩子会在用到这个指令的 dom 插入页面时触发
  inserted(dom, options) {
    // 这里可以拿到使用了这个指令的 dom 其实就是图片 img
    // optionss是指令的表达式
    // 加入img标签出错, 就自动改一个默认地址
    dom.onerror = function() {
      // 如果出错, 就修改默认地址, 但是如果不想每个地方的默认地址都一样
      // 可以在调用指令的时候传值
      dom.src = options.value
    }
  }
}
